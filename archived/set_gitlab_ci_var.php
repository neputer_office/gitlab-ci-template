<?php

echo "Enter Project ID: ";
$project_id = rtrim(fgets(STDIN));

echo "Enter Private Token: ";
$private_token = rtrim(fgets(STDIN));

$file = include("vars.php");
$vars = $file;

# Make the call using API.
foreach ($vars as $var) {
    $ch = curl_init();

    $url = "https://gitlab.com/api/v4/projects/" . $project_id . "/variables";
    curl_setopt($ch, CURLOPT_URL, $url);

    // headers
    $headers = [
        'PRIVATE-TOKEN: '.$private_token,
        'Content-Type: application/json',
    ];
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    curl_setopt($ch, CURLOPT_POST, 1);
    # Setup request to send json via POST.
    $payload = json_encode($var);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // curl_setopt($ch, CURLOPT_VERBOSE, true);

    // Response
    $response = curl_exec($ch);
    curl_close($ch);

    $response = json_decode($response, 1);
    print_r($response);      // print output

    sleep(2);
}
