# Gitlab CI Template



## Getting started

Extend `.gitlab-ci.yml` to your project as:
```
include:
  - remote: https://gitlab.com/neputer_office/gitlab-ci-template/-/raw/master/laravel.gitlab-ci.yml

```

## To create CI variables
1. Edit [vars.php](./vars.php) (if needed)
2. Run php command
```
php set_gitlab_ci_var.php
```
