<?php
// global configs
$set_dev_protected = true;
$set_qa_protected  = true;
$set_staging_protected = true;

$var_key_names = [
    'DESTINATION',
    'SERVER_USER',
    'SERVER_IP',
    'SERVER_PORT',
    'SERVER_PRIVATE_KEY',
    'BRANCH',
    'URL',
];

$environment_keys = [
    //    'dev' => 'DEV',
    'qa' => 'QA',
    // 'staging' => 'STAGE',
    //   'production' => 'PROD'
];

$new_arr = [];
foreach ($environment_keys as $environment_key_ => $environment_key) {

    $set_protected = true;
    if ($environment_key == 'DEV') {
        $set_protected = $set_dev_protected;
    } elseif ($environment_key == 'QA') {
        $set_protected = $set_qa_protected;
    } elseif ($environment_key == 'STAGE') {
        $set_protected = $set_staging_protected;
    }

    foreach ($var_key_names as $var_key_name) {
        $new_arr[$environment_key . '_' . $var_key_name] = [
            "variable_type" => "env_var",
            "key" => $environment_key . '_' . $var_key_name,
            "value" => "",
            "protected" => $set_protected,
            "masked" => false,
            "environment_scope" => $environment_key_,
        ];
    }
}

return array_merge([
    'CUSTOM_GLOBAL_RUNNER_TAG_NAME' => [
        "variable_type" => "env_var",
        "key" => "CUSTOM_GLOBAL_RUNNER_TAG_NAME",
        "value" => "",
        "protected" => false,
        "masked" => false,
        "environment_scope" => "*"
    ]
], $new_arr);
